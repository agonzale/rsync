FROM cern/cc7-base

RUN yum install -y rsync

ENTRYPOINT while true; do sleep 60; done
